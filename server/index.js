import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import {
  registerValidation,
  loginValidation,
  postCreateValidation,
} from "./validations.js";
import checkAuth from "./utils/checkAuth.js";
import { register, getMe, login } from "./controllers/UserController.js";
import {
  create,
  getAll,
  getOne,
  remove,
  update,
} from "./controllers/PostController.js";
import {
  createCategory,
  getCategories,
} from "./controllers/CategoryController.js";
import handleValidationErrors from "./utils/handleValidationErrors.js";

mongoose.connect("mongodb+srv://shengo333:testing123@cluster0.z4dv0h1.mongodb.net/jwtAuth?retryWrites=true&w=majority").then(() => {
  console.log("DB is up");
});

const app = express();
app.use(express.json());
app.use(cors());

app.post("/auth/login/", handleValidationErrors, loginValidation, login);

app.post(
  "/auth/register/",
  handleValidationErrors,
  registerValidation,
  register
);
app.use(checkAuth)
app.post(
  "/posts",
  handleValidationErrors,
  postCreateValidation,
  create
);
app.get("/posts/", getAll);
app.get("/posts/:id/", getOne);
app.delete("/posts/:id/", remove);
app.patch("/posts/:id/", handleValidationErrors, update);

app.post("/category/", createCategory);
app.get("/category/", getCategories);

app.get("/auth/me/", getMe);


app.listen(8080, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("Server is UP");
  }
});
